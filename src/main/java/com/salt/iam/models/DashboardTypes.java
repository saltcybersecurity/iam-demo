package com.salt.iam.models;

public enum DashboardTypes {
    PERSONAL("Personal Dashboard"),
    SALES("Sales Dashboard"),
    HRM("HRM Dashboard"),
    ADMIN("Administrator Dashboard");

    public final String label;

    private DashboardTypes(String label) {
        this.label = label;
    }
}    
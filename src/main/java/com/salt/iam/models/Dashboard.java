package com.salt.iam.models;

import org.springframework.beans.factory.annotation.Value;

public class Dashboard {
    @Value("${UUID.randomUUID()}")
    private String identifier;
    private DashboardTypes dashboardType;

    public DashboardTypes getDashboardType() {
        return dashboardType;
    }

    public void setDashboardType(DashboardTypes dashboardType) {
        this.dashboardType = dashboardType;
    }

    public String getIdentifier() {
        return identifier;
    }

}

package com.salt.iam.config;


import java.net.URI;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
//import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
//import org.springframework.security.oauth2.core.oidc.user.OidcUserAuthority;
//import org.springframework.security.oauth2.core.user.OAuth2UserAuthority;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true)
public class AppConfig extends WebSecurityConfigurerAdapter {

    /* -- OIDC stap 3 -- */

    /* -- OIDC einde stap 3 -- */

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        /* -- OIDC stap 4 -- */

        http    
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/images/**").permitAll()
                .anyRequest().authenticated()
                .and()
                    .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login")
                    .successForwardUrl("/mydashboard")
                    .defaultSuccessUrl("/mydashboard")
                    .permitAll()
                    .and()
                    .exceptionHandling().accessDeniedPage("/accessdenied")
                .and()
                    .logout()
                    .logoutSuccessUrl("/login")
                    .deleteCookies("JSESSIONID")
                    .permitAll()
                .and()
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);

        /* -- OIDC einde stap 4 -- */

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.
            inMemoryAuthentication().
            withUser("basicuser").password(getPasswordEncoder().encode("123")).roles("BASIC_USER");
        auth.
            inMemoryAuthentication().
            withUser("salesuser").password(getPasswordEncoder().encode("123")).roles("BASIC_USER","SALES_USER");
        auth.
            inMemoryAuthentication().
            withUser("hrmuser").password(getPasswordEncoder().encode("123")).roles("BASIC_USER","HRM_USER");
        auth.
            inMemoryAuthentication().
            withUser("admin").password(getPasswordEncoder().encode("123")).roles("BASIC_USER","ADMIN_USER");

    }

    @Bean
    public PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public SimpleUrlAuthenticationSuccessHandler customSuccessHandler() {
        return new SimpleUrlAuthenticationSuccessHandler();
    }
    /* -- OIDC stap 5 --*/
    
    /* -- OIDC einde stap 5 --*/

    /* -- OIDC stap 6 --*/

    /* -- OIDC einde stap 6 --*/

}

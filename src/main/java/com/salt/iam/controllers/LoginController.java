package com.salt.iam.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.salt.iam.models.Credential;


@Controller
public class LoginController {
    /* -- OIDC stap 1 -- */

    /* -- OIDC einde stap 1 -- */


    /* -- OIDC stap 2 -- */
    @GetMapping({"/login"})
    public String getLogin(Credential form, HttpServletResponse response) {
        return "login";
    }

    @PostMapping("/login")
    public String login(BindingResult result) {

        if (result.hasErrors()) {
            return "login";
        }
        return "dashboard";
    }

    /* -- OIDC einde stap 2 -- */
}

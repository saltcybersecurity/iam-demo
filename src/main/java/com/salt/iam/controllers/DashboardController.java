package com.salt.iam.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.annotation.security.RolesAllowed;
import com.salt.iam.models.Dashboard;
import com.salt.iam.models.DashboardTypes;

@Controller
@RolesAllowed("BASIC_USER")
public class DashboardController {
    Logger logger = LogManager.getLogger(DashboardController.class);

    @RequestMapping({"/"})
    public String getIndex(@ModelAttribute Dashboard dashboard, Model model) {
        return getMyDashboard(dashboard, model);
    }

    @RequestMapping("/mydashboard")
    public String getMyDashboard(@ModelAttribute Dashboard dashboard, Model model) {
        dashboard.setDashboardType(DashboardTypes.PERSONAL);
        model.addAttribute("dashboard", dashboard);
        return "dashboard";
    }  

    @RolesAllowed({"SALES_USER","ADMIN_USER"})
    @RequestMapping("/salesdashboard")
    public String getSalesDashboard(@ModelAttribute Dashboard dashboard, Model model) {
        dashboard.setDashboardType(DashboardTypes.SALES);
        model.addAttribute("dashboard", dashboard);
        return "dashboard";
    }  

    @RolesAllowed({"HRM_USER","ADMIN_USER"})
    @RequestMapping("/hrmdashboard")
    public String getHrmDashboard(@ModelAttribute Dashboard dashboard, Model model) {
        dashboard.setDashboardType(DashboardTypes.HRM);
        model.addAttribute("dashboard", dashboard);
        return "dashboard";
    }  

    @RequestMapping("/admindashboard")
    public String getAdminDashboard(@ModelAttribute Dashboard dashboard, Model model) {
        dashboard.setDashboardType(DashboardTypes.ADMIN);
        model.addAttribute("dashboard", dashboard);
        return "dashboard";
    } 
    
    @RequestMapping("/accessdenied")
    public String accessDenied() {
        return "accessdenied";
    }     
    

}
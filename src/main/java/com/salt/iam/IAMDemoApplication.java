package com.salt.iam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IAMDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(IAMDemoApplication.class, args);
	}

}

# Identity Access Management (IAM) 

## Inhoudsopgave
1. [Salt Customer Dashboard](#markdown-header-salt-customer-dashboard)
2. [Voorbereiding](#markdown-header-voorbereiding)
3. [Installatie](#markdown-header-installatie)
4. [Tools](#markdown-header-tools)
5. [Opdrachten](#markdown-header-opdrachten)


## Salt Customer Dashboard
Welkom op het Salt Customer Dashboard. 
Het Salt Customer Dashboard is een applicatie die gebrukt wordt om kwetsbare security configuratie te demonstreren. De applicatie is gebouwd met Spring boot en Thymeleaf als presentatie laag. Het Salt Customer Dashboard maakt gebruikt van Docker en Docker Compose om de omgeving op te zetten.

Het landschap zal bestaan uit 3 docker containers. In het begin zal dit er 1 zijn, die verantwoordelijk is voor de java applicatie. Later voegen we er hier nog 2 aan toe voor Keycloak en Postgres.

![Docker](/readme-images/docker.png)


## Voorbereiding
Voor deze hands-on lab oefening is een docker omgeving met docker-compose nodig.  
Kies één van de volgende opties om dit voor te bereiden. Of sla deze stap over als je al over docker en docker-compose beschikt.  

#### Docker binnen een nieuwe Virtual Machine met Ubuntu Server
* Volg de stappen zoals beschreven in: [ubuntu-server-instructies.md](/ubuntu-server-instructies.md)    

#### Docker binnen een eigen omgeving
* Installeer Docker: [Docker download](https://docs.docker.com/get-docker/)
* Installeer Docker Compose: [Docker compose install](https://docs.docker.com/compose/install/)
  
## Installatie
1. Voer het volgende commando uit: `git clone https://bitbucket.org/saltcybersecurity/iam-demo.git`.
2. Open de folder via het commando `cd iam-demo`.
3. Start Docker.
4. Installeer en run de applicatie. Dit kan op de voorgrond `docker-compose -f docker-compose-app.yml up --build` of achtergrond `docker-compose -f docker-compose-app.yml up --build -d`.
5. Aanpassen hostfile: `[localhost / VP adres] keycloak saltcustomerdashboard`.
6. Openen Salt Customer Dashboard via [saltcustomerdashboard:8580](http://saltcustomerdashboard:8580).

> Tijdens de opdrachten is het nodig om docker opnieuw te laten builden. Hiervoor kan je beter eerst de docker service down brengen. Dit kan met het volgende commando: `docker-compose -f docker-compose-app.yml down`.

Voorbeeld van de Security Demo Applicatie na installatie:

![IAM Demo Login](/readme-images/login.png)

## Tools
1. Code editor ([VS Code](https://code.visualstudio.com/))
2. Recente browser ([EDGE](https://www.microsoft.com/en-us/edge?r=1) / [Chrome](https://www.google.com/chrome/) / [Firefox](https://www.mozilla.org/en-US/firefox/new/))
3. GoogleAuthenticator app (Voor Two Factor Authentication)


## Opdrachten
Voor dit lab hebben we acht verschillende opdrachten klaar staan. Bij het doorlopen van deze opdrachten zullen we het Salt Customer Dashboard aanpassen, zodat deze gebruik zal maken van een IAM oplossing. Het is bij deze opdrachten de bedoeling ze in de aangegeven volgorde uit te voeren. Eventuele antwoorden bij de opdrachten kan je terug vinden in `README-oplossing.md`.

## 1. Huidige applicatie beveiliging
Spring Security is een Java Security framework dat authenticatie en authorisatie aanbiedt.

In het huidige Salt Customer Dashboard maken we gebruiken van verschillenden gebruikers en rollen Hieronder kan je een overzicht zien van deze gebruikers en rollen.

Gebruikers:

* `basicuser`: Maakt gebruikt van de BASIC_USER rol.
* `salesuser`: Maakt gebruikt van de BASIC_USER en SALES_USER rollen.
* `hrmuser`: Maakt gebruikt van de BASIC_USER en HRM_USER rollen.
* `admin`: Maakt gebruikt van de BASIC_USER en ADMIN_USER rollen.
> Al deze gebruikers maken gebruik van het wachtwoord `123`.

Rollen:

* `BASIC_USER`: Heeft toegang tot het /mydashboard, met daarop overzicht van Pentest, Security Scans en Workshops.
* `SALES_USER`: Heeft toegang tot het /salesdashboard, met daarop een overzicht van de sales statistieken.
* `HRM_USER`: Heeft toegang tot het /hrmdashboard, met daarop een overzicht van alle werknemers binnen SALT.
* `ADMIN_USER`: Heeft toegang tot het /admindashboard, met daarop een overzicht van alle gebruikers en tools om deze te bewerken.

Bij het inloggen met één van deze gebruikers die geen beheerder zijn, blijkt dat de rechten op de applicatie niet helemaal goed zijn gezet. Voer de volgende opdrachten uit om de applicatie beter te beveiligen.


#### 1a. Access control op de methodes in de Java controllers van het Dashboard
Er blijkt iets mis te gaan met het admin dashboard. Wanneer gebruikers /admindashboard intypen in de url blijken zij toegang te hebben tot deze pagina. Probeer in de code de admin pagina te beveiligen, zodat deze alleen toegangkelijk is voor de gebruikers met de ADMIN_USER rol.

#### 1b. Beveiliging van de onderdelen van de Dashboard view
Probeer voor deze opdracht ervoor te zorgen dat de hierboven beschreven gebruikers de juiste toegang hebben voor de verschillende blokken op de pagina's.


![White space](/readme-images/white.png)

## 2. Keycloak
Keycloak is een open source identity and access management tool met een focus op Single Page Applications, mobile application en REST api's. 

### 2.1 PostgreSQL
Omdat we keycloak in een docker compose gaan draaien is het belangrijk er een database aan te koppelen voor de opslag van alle aanpassing binnen de applicatie. Anders zou bij elke rebuild van de image de configuratie worden gereset naar de basis instellingen. In dit geval zullen we PostgreSQL gebruiken als database. Deze kunnen we gemakkelijk meegeven als extra docker container in onze docker compose file. PostgreSQL is een opensource SQL database.

#### 2.1a. Opstarten Keycloak en Postgres
Om Keycloak en Postgres toe te voegen aan onze applicatie moeten we een extra docker compose file aanroepen. In deze docker file, `docker-compose-keycloak.yml` kan je eventueel nog het `KEYCLOAK_USER` en `KEYCLOAK_PASSWORD` aanpassen naar eigen wens. Deze USER en PASSWORD worden gebruikt voor het admin account in keycloak. Het commando om deze containers te starten is: `docker-compose -f docker-compose-keycloak.yml up --build`, `-d` kan toegevoegd worden om dit in de achtergrond te draaien. Mocht het nodig zijn om de deze container down te brengen dan kan je gebruiken maken van: `docker-compose -f docker-compose-keycloak.yml down`. Mocht alles goed gaan bij het opstarten, dan zal voor de volgende opdrachten dit commando niet nodig zijn.

Na het succesvol opstarten zou je de keycloak kunnen bekijken op: [http://keycloak:8080/auth/admin](http://keycloak:8080/auth/admin).

Voorbeeld van de Keycloak applicatie na installatie:

![Keycloak Login](/readme-images/keycloak.png)

![White space](/readme-images/white.png)

## 3. Inrichten Keycloak
### 3.1. Realms
Een realm is een ruimte binnen keycloak waar je gebruiker, applicaties, rollen en groepen kan beheren. Zie een realm als een tenant. Het is mogelijk om verschillende realms binnen 1 keycloak installatie te draaien.

#### 3.1a. Aanmaken van een REALM
Log in op [keycloak](http://localhost:8080/auth/admin) met de gebruikersnaam en wachtwoord uit stap 2.
Om een nieuwe Realm aan te maken in keycloak ga je naar de huidge realm, in dit geval is dat `Master`. Als je daar op klikt/hovert met je muis zal er een optie verschijnen om een nieuwe Realm toe te voegen.

![Add realm](/readme-images/realm.png)

Vul hier de naam `IAMDemo` in voor de realm en zorg dat hij enabled is.


### 3.2. Clients
Op een realm kunnen we verschillende clients draaien, dit kan je zien als applicaties of services. Deze kunnen gebruikt worden om gebruikers te authenticeren.

#### 3.2a Aanmaken van een client.
Voor de IAMDemo realm willen we 1 client aanmaken, dit kan je doen door de volgende stappen te volgen:

1. Ga naar clients en klik op de `create` knop. Geef de client de naam `iamdemo-app`.
2. Voor het protocol zullen we `openid-connect` gebruiken.
3. Root en base url kunnen wijzen naar: `http://saltcustomerdashboard:8580`.
4. Na het opslaan moeten we nog 2 dingen aanpassen. Ga hiervoor naar `settings` en zet de Name op `iamdemo-app` en het access type op `confidential`.
5. Onder Client Scopes > Roles > Mappers > realm roles: vinkje aan voor “Add to ID token”. Anders werkt het uitlezen van de rollen niet.



### 3.3. Rollen
Rollen gebruiken we om verschillende users te indentificeren. Als voorbeeld, Admin, User, Manager, Sales etc.. De rollen worden vaak gekoppeld aan een groep, omdat het anders te veel mamangement wordt om dit per gebruiker te doen.

De volgende stap is het aanmaken van de verschillende rollen binnen keycloak. In opdracht 1. hebben we al een overzicht kunnen zien voor de verschillende rollen die nodig zijn in het Salt Customer Dashboard. 

#### 3.3a. Inrichten rollen in keycloak
Probeer voor deze opdracht de 4 rollen toe te voegen aan keycloak. Let hierbij op dat de rollen dezelfde naamgeving gebruiken als die in de applicatie.


### 3.4. Groepen
Groepen worden gebruikt om gebruikers in te delen en te beheren. Op groepen kunnen verschillende attributen of rollen toegepast worden. Wanneer een gebruiker toegevoegd wordt aan een groep zal deze automatische alle rechten en attributen overnemen.

Nu we de verschillende rollen voor de applicatie hebben aangemaakt kunnen we deze toewijzen aan groepen binnen keycloak. Voordat we dit kunnen doen moeten we eerst de verschillende groepen aanmaken. In de huidge sprint boot applicatie maken we geen gebruik van groepen, maar nu we de keycloak intergratie hebben gedaan is het goed om hiermee te werken.

#### 3.4a. Inrichten groepen in keycloak
Maak voor deze opdracht de volgende groepen aan binnen keycloak: `ALL_USERS, SALES_BENELUX, HRM_BENELUX, HRM_NORDICS, GLOBAL_ADMINS`. Probeer daarna voor elke groep de juiste rollen toe te kennen.


### 3.5. Gebruikers.
Nu we de juiste hierarchy en rollen hebben geconfigureerd kunnen we gebruikers gaan toevoegen. 

#### 3.5a. Aanmaken gebruikers in keycloak
Maak voor elke groep binnen keycloak minimaal 1 gebruiker aan die we kunnen gebruiken om de applicatie te testen. Je bent hierin vrij om zelf namen en wachtwoorden te gebruiken.

![White space](/readme-images/white.png)

## 4. Keycloak koppelen aan het Salt Customer Dashboard
In de volgende stappen gaan we de applicatie zo aanpassen dat hij met onze ingerichte keycloak kan werken. Hiervoor zullen we 7 tal aanpassing in de code moeten doen. In iedere stap zal een korte uitleg staan over het aan te passen stuk code. Probeer door de applicatie code heen te lopen om de juiste plekken te vinden waar deze code snippets toegevoegd/aangepast moeten worden. Hiervoor kan je zoeken naar de `<!-- OIDC stap 1 -->` comments.

#### 4a. Stap 0 - Dependencies.
Dit stukje code hoeft niet meer toegevoegd te worden. In verband met alle imports staat deze al aan in de huidige applicatie.
Dit is de generieke Spring Oauth2 client library - we maken geen gebruiken van de specifieke Keycloak libraries. Hierdoor blijft de oplossing ook werken voor andere leveranciers van Oauth2 providers. 

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-oauth2-client</artifactId>
    </dependency>


#### 4b. Stap 1 - Toevoegen van class members aan LoginController class .
Voeg de volgende class members toe aan de LoginController class. Deze worden gebruikt om de login pagina voor te bereiden op alle mogelijke Oauth2 client registraties in deze applicatie. De registratie gebeurt in een latere stap via de application.properties file. 

De ClientRegistrationRepository is een standaard Spring Oauth2 client class die, eenmaal geinstantieerd, de configuratie bevat van alle Oauth2 clients.

    private static String authorizationRequestBaseUri = "oauth2/authorization";
    Map<String, String> oauth2AuthenticationUrls= new HashMap<>();

    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

#### 4c. Stap 2 - Aanpassen van login mapping.
De bestaande login mapping methode stuurt de gebruiker door naar de form-based login pagina. Vervang deze methode door het volgende snippet, zodat er ten eerste alle Oauth2 clients ingeladen worden in een model, en vervolgens de oauth2_login pagina wordt geserveerd. 

    @GetMapping("/login")
    public String getLoginPage(Model model) {
        Iterable<ClientRegistration> clientRegistrations = null;
        ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository)
            .as(Iterable.class);
        if (type != ResolvableType.NONE && 
            ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
            clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
        }
    
        clientRegistrations.forEach(registration -> 
            oauth2AuthenticationUrls.put(registration.getClientName(), 
            authorizationRequestBaseUri + "/" + registration.getRegistrationId()));
        model.addAttribute("urls", oauth2AuthenticationUrls);
    
        return "oauth2_login";
    }

#### 4d. Stap 3 AppConfig bijwerken met ClientRegistrationRepository class member. 
Voeg de volgende class members toe aan de AppConfig class. Hier gebruiken we ook de ClientRegistrationRepository.

    @Autowired
    ClientRegistrationRepository clientRegistrationRepository; 

#### 4e. Stap 4 - AppConfig bijwerken met OAuth2 support in de security configuratie.
De AppConfig is een afgeleid van de `WebSecurityConfigurerAdapter` class. Met deze class kunnen we per inkomend request bepalen of elke gebruiker dit pad mag bekijken of dat hiervoor eerst een authenticatie gedaan moet worden. Folders als images en css zouden altijd moeten worden getoond, terwijl gebruiker specifieke pagina's eerst een ingelogde gebruiker verwachten. In de gedefineerde `configure(HttpSecurity http)` methode, die door Spring standaard wordt aangeroepen, moet de standaard `HttpSecurity http` anders geconfigureerd worden zodat de Oauth2 login en logout gaat werken. Zie hier het snippet:

    http
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/css/**").permitAll()
            .antMatchers("/images/**").permitAll()
            .antMatchers("/login").permitAll()
            .antMatchers("/logout").permitAll()
            .antMatchers("/accessdenied").permitAll()
            .anyRequest().authenticated().and().oauth2Login().loginPage("/login")
            .defaultSuccessUrl("/mydashboard")
            .failureUrl("/accessdenied")
        .and()
            .exceptionHandling().accessDeniedPage("/accessdenied")        
        .and()
            .logout()
            .logoutSuccessHandler(oidcLogoutSuccessHandler())
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) /* nodig om de standaard oauth/logout endpoint te bypassen. Gebruiker logt dan ook uit op Keycloak.*/
            .logoutSuccessUrl("/login")
            .deleteCookies("JSESSIONID")
            .permitAll()
        .and()
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED); 


#### 4f. Stap 5 Realm access rollen vertalen naar Spring Security rollen
De volgende twee methodes moet in AppConfig geplaatst worden. De methode is specifiek bedoeld om de inkomende Keycloak rollen (claims) die in het ID token verpakt zitten van Open ID Connect, te vertalen naar Spring Security rollen.

    @Bean
    public GrantedAuthoritiesMapper userAuthoritiesMapperForKeycloak() {
            return authorities -> {
                Set<GrantedAuthority> mappedAuthorities = new HashSet<>();
                for (GrantedAuthority authority: authorities) {
                    boolean isOidc = authority instanceof OidcUserAuthority;
    
                    if (isOidc) {
                        var oidcUserAuthority = (OidcUserAuthority) authority;
                        var userInfo = oidcUserAuthority.getUserInfo();
        
                        if (userInfo.hasClaim("realm_access")) {
                            var realmAccess = userInfo.getClaimAsMap("realm_access");
                            var roles = (Collection<String>) realmAccess.get("roles");
                            mappedAuthorities.addAll(generateAuthoritiesFromClaim(roles));
                        }
                    } 
                }
                return mappedAuthorities;
            };
        }
    /* Een Spring Security rol heeft altijd de prefix "ROLE_". De echte role correspondeert met de Java @RolesAllowed annotation, bijvoorbeeld in het dashboard. */
    Collection<GrantedAuthority> generateAuthoritiesFromClaim(Collection<String> roles) {
            return roles.stream()
                    .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
                    .collect(Collectors.toList());
    }

#### 4g. Stap 6 - Logout logica toevoegen.
Het laatste stuk code dat aan de AppConfig class toegevoegd moet gaan worden. Deze zorgt ervoor dat, tijdens het de Spring logout functionaliteit, de OIDC logout wordt geïnitieerd vanuit onze applicatie. Dit betekent dat de gebruiker dan ook netjes wordt uitgelogd in Keycloak. 
De Keycloak URL endpoint wordt ingeladen via het zogeheten "well-known" OIDC endpoint van Keycloak. Deze definiëren we later in de application.properties. 


    OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler() { 
        OidcClientInitiatedLogoutSuccessHandler successHandler = new OidcClientInitiatedLogoutSuccessHandler(clientRegistrationRepository);
        successHandler.setPostLogoutRedirectUri("http://saltcustomerdashboard:8580/login");
        return successHandler;
    }


#### 4h. Stap 7 - Keycloak configuratie settings
Er zijn een aantal configuratie settings nodig om de applicatie met de juiste realm  en client te laten praten. Deze settings kunnen we toevoegen aan de application properties. 


Zorg dat je de regel client-secret aanpast naar de juiste secret waarde. Deze kan je terug vinden in keycloak onder: Client > Credentials.

    spring.security.oauth2.client.registration.iamdemo.provider=keycloak
    spring.security.oauth2.client.registration.iamdemo.client-id=iamdemo-app
    spring.security.oauth2.client.registration.iamdemo.client-secret=secret
    spring.security.oauth2.client.registration.iamdemo.authorization-grant-type=authorization_code
    spring.security.oauth2.client.registration.iamdemo.redirect-uri={baseUrl}/login/oauth2/code/
    spring.security.oauth2.client.registration.iamdemo.scope=openid
    spring.security.oauth2.client.registration.iamdemo.client-name=keycloak
    spring.security.oauth2.client.provider.keycloak.issuer-uri=http://keycloak:8080/auth/realms/IAMDemo
    spring.security.oauth2.client.provider.keycloak.userNameAttribute=preferred_username

De issuer-uri is een prefix. Spring Security maakt daar http://keycloak:8080/auth/realms/IAMDemo/.well-known/openid-configuration van. Bekijk deze URL om bijvoorbeeld de UserInfo endpoint te ontdekken.

![White space](/readme-images/white.png)


## 5. One Time Password
Een OTP, of One Time Password wordt gebruikt om diensten in de cloud, persoonlijke credentials en gegevens te beschermen. Meer specifiek gezegd is het een reeks cijfers die automatisch worden gegenereerd om voor één enkele inlogpoging te worden gebruikt. Om deze code te generen kun je gebruik maken van een authenticator zoals GoogleAuthenticator. Keycloak bied ons de mogelijkheid om OTP te aan te zetten voor alle of voor specifieke gebruikers. 

#### 5a. OTP activeren in keycloak
Probeer voor deze opdracht OTP aan te zetten voor alle gebruikers in Keycloak, zodat zij bij het inloggen een authenticator moeten kiezen. Zorg ervoor dat je de GoogleAuthenticator App geinstalleerd hebt op een device.


![White space](/readme-images/white.png)

## 6. Audit trails
Doormiddel van een audit trail (event logging) kan een beheerder achterhalen welke gebruiker op welk moment heeft ingelogd in een applicatie of welke paden door een gebruiker benaderd worden. Door dit in te schakelen wordt het dus lastiger voor een aanvaller om zijn sporen uit te wissen.

#### 6a. Event logging in keycloak
Zet voor deze opdracht event logging aan voor alle gebruikers die inloggen via keycloak.

![White space](/readme-images/white.png)

## 7. Password Policy
Een password policy is een set van regels waaraan een bepaald wachtwoord moet voldoen. Dit wordt vooral gebruikt om zo lastig mogelijke wachtwoorden te genereren die niet door een brute force gekraakt kunnen worden. Denk hierbij aan speciale characters, numbers, hoofdletters of het niet gebruiken van de username in het wachtwoord.

#### 7a. Password Policy in keycloak
Zorg voor een password policy die ervoor zorgt dat een wachtwoord aan de volgende eisen voldoet: expired elke 365 dagen, minimale lengte van 12 charachters, minimaal 1 special character, 1 number, geen username en geen email. Test daarnaa deze policy met het wijzigen van een gebruiker wachtwoord.

![White space](/readme-images/white.png)

## 8. Brute Force
Bij een brute force aanval probeerd een aanvaller doormiddel van verschillende wachtwoorden het juiste wachtwoord te raden. Hiervoor worden vaak automatische scripts gebruikt die lijsten bevatten met vaak gebruikte wachtwoorden. Keycloak bevat een mogelijkheid om dit te kunnen beveiligen. Zo kan je bijvoorbeeld meegeven dat een gebruiker maar 3 keer zijn wachtwoord verkeerd in kan vullen.

#### 8a. Brute Force detectie keycloak
Probeer Brute Force detectie toe te voegen aan de IAMDemo realm. Test daarna of deze goed werkt door een aantal verkeerde wachtwoorden te gebruiken.
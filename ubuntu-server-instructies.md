# Security Configuration Ubuntu Server voorbereidingen

Installeer Virtual Machine met Ubuntu Server vanuit een nieuw image. Deze zijn te downloaden vanaf de [Ubuntu site](https://ubuntu.com/download/server).  
Pak de LTS (Long Term Support) versie. Bij tijd van schrijven (najaar 2021) is dit versie 20.04.3.

Specificaties van de Virtual Machine:  
- 4GB geheugen  
- 2 virtuele CPU's  
- 40GB storage  

Op Windows en Linux zijn bijvoorbeeld VMWare Workstation Player en VirtualBox  geschikt Virtual Machines te configureren en te starten:   
- [VMWare Workstation Player](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html)  
- [VirtualBox](https://www.virtualbox.org/)  

Op de Mac is VMWare Fusion een goede optie:  
- [Fusion](https://customerconnect.vmware.com/web/vmware/evalcenter?p=fusion-player-personal)  

Tijdens de installatie, wordt gevraagd een gebruiker aan te maken. In dit verdere document zal deze gebruiker **salt** zijn.

Nadat de server is geïnstaleerd, open de directe console en doe gelijk een upgrade:  

`sudo apt dist-upgrade`

### Installeer  de net-tools t.b.v. ifconfig

`sudo apt install net-tools`  

Controleer het ip adres:  
`ifconfig -a`

### Installeer de open SSH service:
`sudo apt-get install openssh-server`  
`sudo systemctl enable ssh`  
`vi /etc/ssh/sshd_config`  
  wijzigen: `PermitRootLogin no`  
`service ssh restart`  

Log nu in op de server met je favoriete SSH client op het verkregen ip adres (bijv. met PuTTY) en login met **salt**.

### Installeer Docker:
`sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release`  

`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg`  

`echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"|sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`  

`sudo apt-get update`  
`sudo apt-get install docker-ce docker-ce-cli containerd.io`  

### Test Docker als root:
`sudo docker run hello-world`  

### Docker als non-root docker draaien:
` sudo usermod -aG docker salt`  

### Log uit en opnieuw in met SSH en draai:
`docker run hello-world`  

**Voorbeeld output**  

`salt@vmsecconf:~$ docker run hello-world`  
`Unable to find image 'hello-world:latest' locally`  
`latest: Pulling from library/hello-world`  
`2db29710123e: Pull complete`  
`Digest: sha256:cc15c5b292d8525effc0f89cb299f1804f3a725c8d05e158653a563f15e4f685`  
`Status: Downloaded newer image for hello-world:latest`  
<br>
`Hello from Docker!`  
`This message shows that your installation appears to be working correctly.`  

**Einde voorbeeld output**  

### Installeer Docker Compose:
`sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`  
`sudo chmod +x /usr/local/bin/docker-compose`  
`salt@vmsecconf:~$ docker-compose --version`  

**Voorbeeld output**  

`docker-compose version 1.29.2, build 5becea4c`  

**Einde voorbeeld output**  
## 1a.
@RolesAllowed("ADMIN_USER") Toevegen in DasboardController, voor het admindashboard path.

## 1b.
th:if="${dashboard.dashboardType.name() == 'ADMIN'}" Staat niet op manage users.

## 3.2a.
Ga naar Roles. Klik op Add Role en voer BASIC_USER in. Herhaal voor SALES_USER, HRM_USER en ADMIN_USER.

## 3.3a.
Ga naar Groups. Klik op New en voer ALL_USERS in. Klik daarna op de Groep naam + edit en ga naar de Role Mappings tab. Verplaats BASIC_USER naar het Assigned Roles veld. Herhaal voor SALES_BENELUX, HRM_BENELUX, HRM_NORDICS en GLOBAL_ADMINS.

## 3.4a.
Ga naar Users. Klik op Add User en vul een Username in. Klik op Save en ga daarna naar de Group tab. Selecteer hier de juiste Groepen waar deze gebruiker in kan.

## 5a.
Ga naar Authentication -> Required Action. Vink Default Action aan voor Configure OTP.

## 6a.
Ga naar Events -> Config. Enable login events.

## 7a.
Ga naar Authentication -> Password Policy. Add policy: Expire Password, Minimum Length, Not Username, Not Email, Digits en Special Charachter.

## 8a.
Ga naar Realm Settings -> Security Defenses -> Brute Force Detection. Enable the toggle.